import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import { FaCameraRetro, FaGit, FaPhone, FaEnvelope, FaPython, FaJava, FaGitAlt, FaLinux, FaReact, FaAndroid, FaApple, FaGoogle, FaAngular, FaFlask, FaAmazon } from "react-icons/fa";
import VizSensor from "react-visibility-sensor";
import { Helmet } from "react-helmet";

const TITLE = "Kyle Behiels";

class App extends Component {


  constructor() {
    super();
    this.state = {
      appear_name: "",
      appear_job: "",
      appear_contact: ""
    }

    this.animateSkillChart = this.animateSkillChart.bind(this);
  }
  animateSkillChart(){
    let i = 0;
    let timer = setInterval(() => {
      if(i > 8){
        clearInterval(timer)
      } 
      else{
        console.log("Setting style for " + "sk_" + i);
        let animatables = document.querySelectorAll(".sk_" + i);
        animatables.forEach(skill => {
          skill.style.animation = "300ms ease-out fadeIn";
          skill.style.animationFillMode = "forwards";
        });
      }
      i++;
    }, 300)
  }

  render() {
    return (
      <div className="App">
        <Helmet>
          <title>{ TITLE }</title>
        </Helmet>
        
        <div className="app-disclaimer">
          <p>In Development: This page is still <b>under development</b>, far from mobile friendly and is intended to serve as a fun showcase as opposed to a comprehensive resume. For that, please see: <a href="https://gitlab.com/kylejamesbehiels/profile-website/-/tree/master/src/Resources/KyleBehiels_Resume.pdf" download="KyleBehiels_Resume.pdf">My Resume</a></p>
        </div>
        <div className="custom-jumbotron">
          <div className="jumbotron-left">
            <h1 id="my_name">Kyle Behiels</h1>  <hr></hr> <h2 id="my_job">Software Developer</h2>
          </div>
          <div className="jumbotron-right">
            <p id="my_contact">
              +1-(587)-785-5528 : <FaPhone></FaPhone> <br></br>
              kylejamesbehiels@gmail.com : <FaEnvelope></FaEnvelope> <br></br>
              gitlab.com/kylejamesbehiels : <FaGit></FaGit><br></br>
            </p>
          </div>
        </div>
        <div id="navigation_wrapper">
          <div id="navigation">
            <div className="nav-btn">About Me</div>
            <div className="nav-btn">Technologies</div>
            <div className="nav-btn">Research</div>
            <div className="nav-btn">Projects</div>
            <div className="nav-btn">Hobbies</div>
          </div>
        </div>
        <div className="content">
          <div id="about_div">
            <p><h2>About Me</h2></p>
            <p>Welcome! My name is Kyle Behiels and I am a software developer from Edmonton, Alberta, Canada. To learn more, keep scrolling or select any of the links above! <br></br><br></br> <FaCameraRetro></FaCameraRetro> Luxembourg City, Luxembourg</p>
          </div>
        </div>
        <div className="skills_div section_div">
          <h2 className="section_title">Technologies</h2>
          <div id="skills">
            <div id="languages" className="skill-div">
              <span className="skill sk_8"><FaPython></FaPython> Python</span> <br></br>
              <span className="skill sk_7"><FaJava></FaJava>Java</span> <br></br>
              <span className="skill sk_6">Javascript</span> <br></br>
              <span className="skill sk_5">C#</span> <br></br>
              <span className="skill sk_4">ES6</span> <br></br>
              <span className="skill sk_3"><FaGitAlt></FaGitAlt> Git</span> <br></br>
              <span className="skill sk_2">Bash</span> <br></br>
              <span className="skill sk_1">Latex</span> <br></br>
            </div>
            <div id="front_end" className="skill-div">
              <span className="skill_hidden">blank</span> <br></br>
              <span className="skill_hidden">blank</span> <br></br>

              <span className="skill_hidden">blank</span> <br></br>
              <span className="skill sk_5"><FaAngular></FaAngular> Angular</span> <br></br>
              <span className="skill sk_4"><FaReact></FaReact> React</span> <br></br>
              <span className="skill sk_3">React Native</span> <br></br>
              <span className="skill sk_2"><FaAndroid></FaAndroid> Android Studio</span> <br></br>
              <span className="skill sk_1"><FaApple></FaApple> X-Code</span> <br></br>
            </div>
            <div id="back_end" className="skill-div">
              <span className="skill_hidden">blank</span> <br></br>
              <span className="skill_hidden">blank</span> <br></br>
              <span className="skill sk_6 ">Flask</span> <br></br>
              <span className="skill sk_5">Django</span> <br></br>
              <span className="skill sk_4"> <FaGoogle></FaGoogle> Google Suite</span> <br></br>
              <span className="skill sk_3">IBM Suite</span> <br></br>
              <span className="skill sk_2"><FaAmazon></FaAmazon> AWS</span> <br></br>
              <span className="skill sk_1">Deeplearning4j</span> <br></br>
            </div>
            <div id="dev_ops" className="skill-div">
              <span className="skill_hidden">blank</span> <br></br>
              <span className="skill_hidden">blank</span> <br></br>
              <span className="skill_hidden ">blank</span> <br></br>
              <span className="skill_hidden">blank</span> <br></br>
              <span className="skill sk_4">Circle CI</span> <br></br>
              <span className="skill sk_3">Jira</span> <br></br>
              <span className="skill sk_2">Virtualization</span> <br></br>
              <span className="skill sk_1">Docker</span> <br></br>
            </div>
          </div>
          <VizSensor onChange={(isVisible) => {
            if(isVisible){
              this.animateSkillChart()
            }
            }}>
          <hr></hr>
          </VizSensor>
          <div id="labels">
            <div className="label">
              Languages & <br></br> Technologies
            </div>
            <div className="label">
              Front-End
            </div>
            <div className="label">
              Back-End
            </div>
            <div className="label">
              Dev-Ops
            </div>
          </div>
        </div>
        <div className="research_div section_div">
          <h2 className="section_title">Research</h2>
          <div id="reddit_card" className="research-card">
            <div className="research-card-content">
              <h2 className="research-card-title">Linguistic Profiling of Reddit Users with Neural Networks for the Purpose of Authentication and Verification</h2>
              <hr></hr>

              <p className="research-card-abstract"> <b>Abstract: </b>The age of information has come and gone. In its place, the age of disinformation. In today’s volatile global markets, deception, misrepresentation, and outright dishonesty exist everywhere we look. Information technology is only as good as the information it delivers, and in recent years propaganda farms and social media impersonators have obfuscated the truth so completely that no one can trust anything they read online without cautious source checking. This paper discusses a novel approach at identifying real Reddit users based on the structural breakdown of their comments. Using two machine learning models, Regression Neural Network (RNN) and Convolutional Neural Network (CNN), we were able to positively identify users, with an average accuracy of 81.304\% and 80.004\% respectively. Looking forward, our identification model could be used as an early intrusion detection tool or to determine legitimacy of information based on the user in question.</p>
              <a className="paper-link" href="#">Link to Paper</a>
            </div>
            <div></div>
          </div>
          <div id="eisi_card" className="research-card">
            <div className="research-card-content">
              <h2 className="research-card-title">Analysis of Vancouver Crime and Census Data Using Various Machine Learning Algorithms (UREAP)</h2>
              <hr></hr>

              <p className="research-card-abstract"> <b>Abstract: </b>In recent years mass storage of criminal data has become a common practice for many law enforcement agencies around the world. As these data sets grow, so does the amount of potential knowledge that we can gain from analyzing these data. When combined with census data and the open crime data set of Vancouver (non-violent crimes from between 2003 and 2019), we have a unique opportunity to explore the spatio-temporal snapshot of crime rate and causation. The resulting data lends itself extremely well to the application of various machine learning algorithms. In this paper, we employs various machine learning algorithms to show the attributes correlation with most strongly with both high and low crime rates.</p>
              <a className="paper-link" href="#">Link to Paper</a>
            </div>
            <div></div>
          </div>
          <div id="isi_card" className="research-card">
            <div className="research-card-content">
              <h2 className="research-card-title">Discovering Crime Trends and Patterns Using Three-Dimensional Visual Analytics</h2>
              <hr></hr>

              <p className="research-card-abstract"> <b>Abstract: </b>Crime analysts and law enforcement agencies can formulate effective strategies utilizing current crime trends and patterns. Modern computational technologies, particularly big data analysis and visualization can be applied to available crime data and other resources. Visual analytics, an analytical reasoning technique with interactive visual interfaces, is a powerful tool that serves to augment the trend and pattern recognition capabilities of experts. This paper introduces a three-dimensional (3D) visual analytics framework that interactively visualizes crime data and other relevant datasets on a highly accurate 3D model of the City of Vancouver, Canada. This 3D visualization advances crime analysis activities through a more accurate display of the area under study. The 3D visual analytics can improve identification of temporal and spatial criminal areas and provide strategic and effective plans for pro-active police deployment. The open data catalogue from the City of Vancouver, Canada and the Vancouver Police Department open source property crime data were used in this study</p>
              <a className="paper-link" href="#">Link to Paper</a>
            </div>
            <div></div>
          </div>
        </div>
      </div>
    );
  }

}

export default App;
